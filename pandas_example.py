import pandas as pd
import pickle as pl
import json

SOURCE_DATASET_PATH = 'data/source_dataset.csv'
RESULT_DATASET_PATH = 'data/result_dataset.csv'
SOURCE_CORPUS_PATH = 'data/source_texts.pickle'
RESULT_CORPUS_PATH = 'data/result_texts.pickle'
BATCHED_DATA_PATH = 'data/batched_data.pickle'
TMP_FILE_PATH = 'data/tmp'
MAX_OUTPUT = 10
BATCH_SIZE = 100


'''
Decorator just for beauty output in terminal.
'''
def start_finish_decorator(example_function):
    def decorated_function():
        print('\n\t- Start  {}.'.format(example_function.__name__))
        example_function()
        print('\t- Finish {}.'.format(example_function.__name__))

    return decorated_function


'''
Simple example read/write Pickle.
'''

def read_pickle_corpus(corpus_path=SOURCE_CORPUS_PATH):
    with open(corpus_path, 'rb') as corpus_file:
        corpus = pl.load(corpus_file)

    return corpus


def write_pickle_corpus(corpus, corpus_path=RESULT_CORPUS_PATH):
    with open(corpus_path, 'ab+') as corpus_file:
        pl.dump(corpus, corpus_file)


@start_finish_decorator
def example_read_write_pickle():
    corpus = read_pickle_corpus()

    write_pickle_corpus(corpus)


'''
Simple example read/write csv dataset with Pandas.
'''

def read_dataset(dataset_path=SOURCE_DATASET_PATH):
    return pd.read_csv(dataset_path)


def write_dataset(dataset, dataset_path=RESULT_DATASET_PATH):
    dataset.to_csv(dataset_path)


@start_finish_decorator
def example_read_write_pandas():
    dataset = read_dataset()

    write_dataset(dataset)


'''
Multiple read/write with Pickle.
'''

@start_finish_decorator
def example_multiple_read_write_pickle():
    corpus = read_pickle_corpus()

    with open(TMP_FILE_PATH, 'ab+') as tmp_file:
        for text in corpus:
            pl.dump(text, tmp_file)


    with open(TMP_FILE_PATH, 'rb') as tmp_file:
        for _ in range(min(len(corpus), MAX_OUTPUT)):
            print(pl.load(tmp_file))


'''
Batching with Pickle.
'''

def make_batch_list(corpus):
    batch_list = []
    batch = []
    for id, text in enumerate(corpus):
        batch.append(text)

        if (id % BATCH_SIZE == BATCH_SIZE - 1) or id == len(corpus) - 1:
            batch_list.append(batch)
            batch = []

    return batch_list


@start_finish_decorator
def example_batching_pickle():
    corpus = read_pickle_corpus()

    batch_list = make_batch_list(corpus)

    with open(BATCHED_DATA_PATH, 'ab+') as batched_data_file:
        for batch in batch_list:
            pl.dump(batch, batched_data_file)

'''
Batching with Pandas.
'''

@start_finish_decorator
def example_batching_pandas():
    dataset = read_dataset()

    corpus = list(dataset['text'].array)

    batch_list = make_batch_list(corpus)

'''
Main function. Code starts here.
'''
def main():
    print('Some examples of code with Pandas.')

    example_read_write_pickle()
    example_read_write_pandas()
    example_multiple_read_write_pickle()
    example_batching_pickle()
    example_batching_pandas()

if __name__ == '__main__':
    main()
